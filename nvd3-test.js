Bars = new Meteor.Collection("bars");

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

if (Meteor.isClient) {
  Template.hello.rendered = function() {
    var exampleData = [{
      key: "Cumulative Return",
      values: Bars.find().fetch()
    }];

    var chart = nv.models.discreteBarChart()
      .x(function(d) { return d.label; })    //Specify the data accessors.
      .y(function(d) { return d.value; })
      .tooltips(true)        //Don't show tooltips
      .duration(350)
      ;
    d3.select('#chart svg')
      .datum(exampleData)
      .call(chart);

    nv.utils.windowResize(chart.update);

    // Push the entire collection via autorun. If finer-grain control is necessary,
    // using observeChanges will probably do the trick. But, D3 is smart enough to
    // only update DOM elements that themselves have changed, so to some degree
    // optimization is taken care of already.
    //
    // Bars.find().observeChanges({
    //   added: function (id, fields) {
    //     // ...
    //   },
    //   changed: function (id, fields) {
    //     // ...
    //   },
    //   removed: function (id) {
    //     // ...
    //   }
    // });

    this.autorun(function () {
      exampleData[0].values = Bars.find().fetch();
      d3.select('#chart svg')
        .datum(exampleData)
        .call(chart);
    });

  };

  Template.hello.events({
    'click #addDataButton': function() {
      var height = getRandomInt(13, 89);
      var lastBar = Bars.findOne({}, {fields:{'label':1},sort:{'label':-1},limit:1,reactive:false});
      if (lastBar) {
        Bars.insert({'label': (lastBar.label + 1), 'value': height});
      } else {
        Bars.insert({'label':1, 'value':height});
      }
    },
    'click #removeDataButton': function() {
      var lastBar = Bars.findOne({}, {fields:{'label':1},sort:{'label':-1},limit:1,reactive:false});
      if (lastBar) {
        Bars.remove(lastBar._id);
      }
    }
  });
}
