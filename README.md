Basic NVD3 bar graph example in Meteor.

See [meteor-reactive-nvd3-js-graph](https://gitlab.com/meonkeys/meteor-reactive-nvd3-js-graph) for inspiration.
